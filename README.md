# java-fx-cipher-tool

#### 介绍
使用JavaFx编写的加解密工具

#### 功能介绍

目前支持base64、aes。功能见下方图片

![](doc/3814E6EB-9F93-4E5D-85C3-20AB25C3EE03.png)

![](doc/600336E1-0B6C-4565-9F6A-4421A3C477FE.png)

![](doc/41E3796D-24FA-4DB6-BB96-DC160B15DC30.png)

#### 更新情况
##### v1.1

1. add rsa


##### v1.0
1. base64
2. aes/cbc/pkcs5padding
#### 开发环境
  1. idea 2019.2
  2. jdk 8
  
#### 工程初始化
刚刚下载好工程后，project级别的sdk可能不是jdk8，手动设置一下就好了。

![](doc/A30F5157-3265-4D6D-9AE8-FE9C45B999DA.png)

#### 运行程序

直接运行 类 Main中的main方法

#### 构建本地安装程序

```
mvn jfx:native
```