package com.jijunpeng.javafx.cipher.persistence;

import java.lang.annotation.*;

/**
 * 记录持久化json的路径
 *
 * @author Ji Junpeng
 * @date 2019-12-13 00:03
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD, ElementType.TYPE})
@Repeatable(PersistenceJson.List.class)
public @interface PersistenceJson {
    /**
     * 保存配置的json路径
     * @return
     */
    String path();

    String field() default "";

    String getter() default "";

    String setter() default "";

    @Documented
    @Retention(RetentionPolicy.RUNTIME)
    @Target({ElementType.FIELD, ElementType.TYPE})
    @interface List{
        PersistenceJson[] value();
    }
}
