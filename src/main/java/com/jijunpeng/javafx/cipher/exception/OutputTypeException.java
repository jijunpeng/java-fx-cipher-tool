package com.jijunpeng.javafx.cipher.exception;

/**
 * @author Ji Junpeng
 * @date 2019-11-30 19-13
 */
public class OutputTypeException extends Exception {
    public OutputTypeException() {
        super("输出类型错误");
    }

    public OutputTypeException(String message) {
        super(message);
    }

    public OutputTypeException(String message, Throwable cause) {
        super(message, cause);
    }

    public OutputTypeException(Throwable cause) {
        super(cause);
    }

    public OutputTypeException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
