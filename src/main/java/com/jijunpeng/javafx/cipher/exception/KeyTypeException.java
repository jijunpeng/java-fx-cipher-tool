package com.jijunpeng.javafx.cipher.exception;

/**
 * @author Ji Junpeng
 * @date 2019-11-30 19-13
 */
public class KeyTypeException extends Exception {
    public KeyTypeException() {
        super("密钥类型错误");
    }

    public KeyTypeException(String message) {
        super(message);
    }

    public KeyTypeException(String message, Throwable cause) {
        super(message, cause);
    }

    public KeyTypeException(Throwable cause) {
        super(cause);
    }

    public KeyTypeException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
