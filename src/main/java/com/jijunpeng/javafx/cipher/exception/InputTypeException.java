package com.jijunpeng.javafx.cipher.exception;

/**
 * @author Ji Junpeng
 * @date 2019-11-30 19-13
 */
public class InputTypeException extends Exception {
    public InputTypeException() {
        super("输入类型错误");
    }

    public InputTypeException(String message) {
        super(message);
    }

    public InputTypeException(String message, Throwable cause) {
        super(message, cause);
    }

    public InputTypeException(Throwable cause) {
        super(cause);
    }

    public InputTypeException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
