package com.jijunpeng.javafx.cipher.common.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.apache.commons.codec.Charsets;

import java.nio.charset.Charset;

/**
 * @author Ji Junpeng
 * @date 2019-12-01 00-34
 */
@Getter
@AllArgsConstructor
public enum CharsetEnum {
    /**
     * UTF-8
     */
    UTF_8(Charsets.UTF_8),
    GB2312(Charset.forName("GB2312")),
    ;
    private final Charset charset;

    @Override
    public String toString() {
        return this.charset.displayName();
    }
}
