package com.jijunpeng.javafx.cipher.common.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author Ji Junpeng
 * @date 2019-12-01 02-04
 */
@Getter
@AllArgsConstructor
public enum RsaPaddingEnum {
    /**
     * pkcs1
     */
    PKCS1("PKCS1Padding"),
    PKCS1_OAEP("PKCS1OaepPadding"),
    SSL_V23("SSLV23Padding"),
    ;
    private final String desc;

    @Override
    public String toString() {
        return this.desc;
    }
}
