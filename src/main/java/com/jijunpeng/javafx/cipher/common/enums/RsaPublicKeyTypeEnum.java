package com.jijunpeng.javafx.cipher.common.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author Ji Junpeng
 * @date 2019-12-01 00-30
 */
@Getter
@AllArgsConstructor
public enum RsaPublicKeyTypeEnum {
    /**
     * 字符串形式的密钥
     */
    STRING("String"),
    /**
     * base64形式的密钥
     */
    BASE64("Base64"),
    /**
     * hex形式的密钥
     */
    HEX("Hex"),
    ;
    private final String desc;

    @Override
    public String toString() {
        return this.desc;
    }
}
