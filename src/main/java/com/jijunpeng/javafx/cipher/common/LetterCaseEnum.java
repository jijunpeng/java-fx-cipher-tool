package com.jijunpeng.javafx.cipher.common;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * @author Ji Junpeng
 * @date 2019-12-10 23:58
 */
@Getter
@RequiredArgsConstructor
public enum LetterCaseEnum {
    /**
     * 小写
     */
    LOWER("小写"),
    UPPER("大写"),
    ;
    private final String desc;

    @Override
    public String toString() {
        return this.desc;
    }
}
