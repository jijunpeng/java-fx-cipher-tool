package com.jijunpeng.javafx.cipher.common;

/**
 * @author Ji Junpeng
 * @date 2019-11-30 18-22
 */
public interface ComboTxtConstants {
    String CHARSET_UTF_8 = "UTF-8";
    String CHARSET_GB2312 = "GB2312";
    String[] LIST_CHARSET = {CHARSET_UTF_8, CHARSET_GB2312,};

    String AES_ENCRYPT_MODE_CBC = "CBC";
    String AES_ENCRYPT_MODE_ECB = "ECB";
    String AES_ENCRYPT_MODE_CTR = "CTR";
    String AES_ENCRYPT_MODE_OFB = "OFB";
    String AES_ENCRYPT_MODE_CFB = "CFB";

    String[] LIST_AES_ENCRYPT_MODE = {AES_ENCRYPT_MODE_CBC, AES_ENCRYPT_MODE_ECB, AES_ENCRYPT_MODE_CTR, AES_ENCRYPT_MODE_OFB, AES_ENCRYPT_MODE_CFB,};

    String AES_PADDING_PKCS5_PADDING = "PKCS5Padding";
    String AES_PADDING_PKCS7_PADDING = "PKCS7Padding";
    String AES_PADDING_ZERO_PADDING = "ZeroPadding";
    String AES_PADDING_ISO10126_PADDING = "ISO10126Padding";
    String AES_PADDING_X923_PADDING = "X9.23Padding";
    String AES_PADDING_NO_PADDING = "NoPadding";

    String[] LIST_AES_PADDING = {AES_PADDING_PKCS5_PADDING, AES_PADDING_PKCS7_PADDING, AES_PADDING_ZERO_PADDING, AES_PADDING_ISO10126_PADDING, AES_PADDING_X923_PADDING, AES_PADDING_NO_PADDING,};

    String AES_PROVIDER_JDK = "JDK";
    String AES_PROVIDER_BC = "Bouncy Castle";

    String[] LIST_AES_PROVIDER = {AES_PROVIDER_BC, AES_PROVIDER_JDK,};

    String AES_KEY_TYPE_STRING = "String";
    String AES_KEY_TYPE_HEX = "Hex";
    String AES_KEY_TYPE_BASE64 = "Base64";

    String[] LIST_AES_KEY_TYPE = {AES_KEY_TYPE_STRING, AES_KEY_TYPE_HEX, AES_KEY_TYPE_BASE64,};
}
