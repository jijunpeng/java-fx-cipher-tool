package com.jijunpeng.javafx.cipher.common;

import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.layout.Pane;
import javafx.stage.Window;

/**
 * @author Ji Junpeng
 * @date 2019-11-30 17-41
 */
public class DialogUtil {
    private DialogUtil() {
    }

    public static Alert showError(Pane rootPane, String title, String message) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        if (rootPane != null) {
            Scene scene = rootPane.getScene();
            if (scene != null) {
                Window window = scene.getWindow();
//                double rootX = window.getX();
//                double rootY = window.getY();
//                double width = window.getWidth();
//                double height = window.getHeight();
//                double alertX = rootX + width / 2 - alert.getWidth() / 2;
//                double alertY = rootY + height / 2 - 110;
//                alert.setX(alertX);
//                alert.setY(alertY);
                alert.initOwner(window);
            }
        }
        alert.setTitle(title);
        alert.setContentText(message);
        alert.showAndWait();
        return alert;
    }
}
