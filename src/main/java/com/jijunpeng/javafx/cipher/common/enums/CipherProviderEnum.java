package com.jijunpeng.javafx.cipher.common.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author Ji Junpeng
 * @date 2019-12-01 02-08
 */
@Getter
@AllArgsConstructor
public enum CipherProviderEnum {
    /**
     * 好用的第三方加密
     */
    BOUNCY_CASTLE("Bouncy Castle"),
    /**
     * JDK自带加密
     */
    JDK("JDK"),
    ;
    private final String desc;
}
