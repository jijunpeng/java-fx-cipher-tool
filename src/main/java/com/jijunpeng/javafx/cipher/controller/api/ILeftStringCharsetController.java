package com.jijunpeng.javafx.cipher.controller.api;

import com.jijunpeng.javafx.cipher.common.enums.CharsetEnum;
import javafx.collections.FXCollections;
import javafx.scene.control.ComboBox;
import javafx.scene.control.RadioButton;

/**
 * @author Ji Junpeng
 * @date 2019-12-01 01-11
 */
public interface ILeftStringCharsetController {

    RadioButton getLeftStringRb();

    ComboBox<CharsetEnum> getLeftStringCharsetComboBox();

    default void initLeftStringCharset() {
        getLeftStringCharsetComboBox().setItems(FXCollections.observableArrayList(CharsetEnum.values()));
        getLeftStringCharsetComboBox().setValue(CharsetEnum.values()[0]);
        getLeftStringRb().selectedProperty().addListener((observable, oldValue, newValue) -> getLeftStringCharsetComboBox().setDisable(!newValue));
    }
}
