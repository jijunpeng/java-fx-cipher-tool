package com.jijunpeng.javafx.cipher.controller;

import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.JSONPath;
import com.jijunpeng.javafx.cipher.controller.api.BaseController;
import com.jijunpeng.javafx.cipher.persistence.PersistenceJson;
import javafx.fxml.FXML;
import javafx.scene.control.TabPane;
import javafx.scene.layout.Pane;

/**
 * @author Ji Junpeng
 * @date 2019-11-30 15:01
 */
@PersistenceJson(path = "$.main")
public class MainController extends BaseController {
    @FXML
    private Pane mainRootPane;
    @FXML
    private TabPane tabPane;

    @Override
    protected Pane getRootPane() {
        return this.mainRootPane;
    }

    @Override
    public JSONObject persistWidgets() throws IllegalAccessException {
        JSONObject jsonObject = super.persistWidgets();
        JSONPath.set(jsonObject, "$.main.tabPane.index", getTabPaneSelectedIndex());
        return jsonObject;
    }

    @Override
    protected void initPersistedValue(JSONObject persistenceJson) throws IllegalAccessException {
        super.initPersistedValue(persistenceJson);
        Integer tabIndex = (Integer) JSONPath.eval(persistenceJson, "$.main.tabPane.index");
        if (tabIndex != null) {
            tabPane.getSelectionModel().select(tabIndex);
        }
    }

    @Override
    protected void initViewData() {
        super.initViewData();
//        mainRootPane.sceneProperty().addListener(new ChangeListener<Scene>() {
//            @Override
//            public void changed(ObservableValue<? extends Scene> observable, Scene oldValue, Scene newValue) {
//                System.err.println("........");
//                mainRootPane.getScene().setOnKeyPressed(new EventHandler<KeyEvent>() {
//                    @Override
//                    public void handle(KeyEvent event) {
//                        if (event.isControlDown() && event.getCode() == KeyCode.TAB) {
//                            int index = getTabPaneSelectedIndex();
//                            if (event.isShiftDown()) {
//                                if (index <= 0) {
//                                    setTabPaneSelectedIndex(getTabPaneSize() - 1);
//                                } else {
//                                    setTabPaneSelectedIndex(index - 1);
//                                }
//                            } else {
//                                if (index >= getTabPaneSize() - 1) {
//                                    setTabPaneSelectedIndex(0);
//                                } else {
//                                    setTabPaneSelectedIndex(index + 1);
//                                }
//                            }
//                        }
//                    }
//                });
//            }
//        });
    }

    public int getTabPaneSelectedIndex() {
        return tabPane.getSelectionModel().getSelectedIndex();
    }

    public void setTabPaneSelectedIndex(int index) {
        tabPane.getSelectionModel().select(index);
    }

    public int getTabPaneSize() {
        return tabPane.getTabs().size();
    }
}
