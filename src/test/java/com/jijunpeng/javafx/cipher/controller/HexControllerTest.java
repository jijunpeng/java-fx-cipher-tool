package com.jijunpeng.javafx.cipher.controller;

import cn.hutool.core.util.ReflectUtil;
import com.jijunpeng.javafx.cipher.common.ComboTxtConstants;
import org.junit.Before;
import org.junit.Test;

import java.nio.charset.Charset;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * @author Ji Junpeng
 * @date 2019-11-30 18-14
 */
public class HexControllerTest {
    private HexController hexController;
    @Before
    public void before() {
        hexController = new HexController();
    }
    @Test
    public void testEncodeStr() {
        {
            String hexStr = ReflectUtil.invoke(hexController, "encodeStr", "你好", Charset.forName(ComboTxtConstants.CHARSET_UTF_8));
            assertThat(hexStr).isEqualToIgnoringCase("e4bda0e5a5bd");
        }
        {
            String hexStr = ReflectUtil.invoke(hexController, "encodeStr", "你好", Charset.forName(ComboTxtConstants.CHARSET_GB2312));
            assertThat(hexStr).isEqualToIgnoringCase("c4e3bac3");
        }
    }

    @Test
    public void testDecodeHex() {
        {
            String hexStr = ReflectUtil.invoke(hexController, "decodeHex", "e4bda0e5a5bd", Charset.forName(ComboTxtConstants.CHARSET_UTF_8));
            assertThat(hexStr).isEqualToIgnoringCase("你好");
        }
        {
            String hexStr = ReflectUtil.invoke(hexController, "decodeHex", "c4e3bac3", Charset.forName(ComboTxtConstants.CHARSET_GB2312));
            assertThat(hexStr).isEqualToIgnoringCase("你好");
        }
    }
}