package com.jijunpeng.javafx.cipher.controller;

import com.jijunpeng.javafx.cipher.common.ComboTxtConstants;
import org.junit.Test;

import java.nio.charset.Charset;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * @author Ji Junpeng
 * @date 2019-11-30 19-07
 */
public class BaseTest {
    @Test
    public void testCharsetList() {
        for (String charsetStr : ComboTxtConstants.LIST_CHARSET) {
            Charset charset = Charset.forName(charsetStr);
            assertThat(charset).isNotNull();
        }
    }
}
