package com.jijunpeng.javafx.cipher.controller;

import cn.hutool.crypto.symmetric.AES;
import org.apache.commons.codec.Charsets;
import org.apache.commons.codec.binary.Base64;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * @author Ji Junpeng
 * @date 2019-11-30 22-56
 */
public class AesTest {
    @Test
    public void testAesCbcPkcs5Padding() {
        String plainText = "hello";
        String key = "12345678901234567890123456789012";
        String iv = "1234567890123456";
        AES aes = new AES("CBC", "PKCS5Padding", key.getBytes(Charsets.UTF_8), iv.getBytes(Charsets.UTF_8));
        byte[] encryptBytes = aes.encrypt(plainText.getBytes(Charsets.UTF_8));
        assertThat(Base64.encodeBase64String(encryptBytes)).isEqualToIgnoringCase("RaNZPR8efmmeJt+ylF1BuQ==");
        assertThat(new String(aes.decrypt(Base64.decodeBase64("RaNZPR8efmmeJt+ylF1BuQ==")))).isEqualTo("hello");
    }
}
